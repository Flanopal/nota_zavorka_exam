function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Collect metal from features",
		parameterDefs = {
			{ 
				name = "collector",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "radius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


local Order = Spring.GiveOrderToUnit

local function ClearState(self)
end

-- @load unit
function Run(self, units, parameter)
	local collector = parameter.collector
	local position = parameter.position
	local radius = parameter.radius

	if self.started ~= true then
		Order(collector, CMD.RECLAIM,{position.x, position.y, position.z, radius},{})
		self.started = true
	end

	if #Sensors.nota_zavorka_exam.GetTrashAtLinePoint(position, radius) == 0 then
		self.started = nil
		return SUCCESS
	end
	return RUNNING
end

function Reset(self)
	ClearState(self)
end

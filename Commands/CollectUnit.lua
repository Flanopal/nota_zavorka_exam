function getInfo()
	return {
		onNoUnits = SUCCESS,
		tooltip = "Recycle unit",
		parameterDefs = {
			{ 
				name = "collector",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end


local Order = Spring.GiveOrderToUnit

local function ClearState(self)
end

-- @recycle unit
function Run(self, units, parameter)
	local collector = parameter.collector
	local target = parameter.unit

	if self.started ~= true then
		Order(collector, CMD.RECLAIM,{target},{})
		self.started = true
	end

	if Spring.ValidUnitID(target) then
		return RUNNING
	end
	self.started=nil
	return SUCCESS
end

function Reset(self)
	ClearState(self)
end

local sensorInfo = {
	name = "Assign unit to lane",
    desc = "Passed unit is assigned to lane into proper place in structure",
	author = "Flanopal"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(buyRequest, laneUnits,id)
	if id == nil then
		return nil
	end

	local path = buyRequest.path
	local position = laneUnits
	for i=1,#path - 1 do
		position = position[path[i]]
	end
	position[path[#path]] = id
end
local sensorInfo = {
	name = "Is unit class",
	desc = "Check if unit belongs to given class",
	author = "Flanopal",
	date = "2019-05-09"
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(unit, class)
	if unit == nil then
		return false
	end

	local unitDefID = Spring.GetUnitDefID(unit)
	local name = UnitDefs[unitDefID].name
	return name == class
end
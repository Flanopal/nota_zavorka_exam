local sensorInfo = {
	name = "InitLineInfo",
	desc = "Initialize info about given line",
	author = "Flanopal",
	date = "2019-05-09"
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function()
	local lineInfo = {}
	lineInfo["lane"] = 1
	lineInfo["target"] = 1

	local enemyTeams = {}
	local teams = Spring.GetTeamList()
	local allies = Spring.GetAllyTeamList()

	for i=1,#teams do
		local ally = false
		for j=1,#allies do
			if teams[i] == allies[j] then
				ally = true
				break
			end
		end

		if ally == false then
			enemyTeams[teams[i]] = 1
		end
	end

	lineInfo["enemyTeams"] = enemyTeams

	return lineInfo
end
local sensorInfo = {
	name = "Init units",
	desc = "Init structure that is used for units of the lane",
	author = "Flanopal",
	date = "2019-05-09"
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function()
	local unitsField = {}

	unitsField["farcs"] = {}

	unitsField["squads"] = {}
	unitsField["squads"]["units"] = {}
	unitsField["squads"]["artilery"] = {}

	unitsField["defenders"] = {}
	unitsField["defenders"]["units"] = {}
	for i = 1,5 do
		unitsField["defenders"]["units"][i] = {}
	end

	unitsField["defenders"]["towers"] = {}
	for i = 1,4 do
		unitsField["defenders"]["towers"][i] = {}
	end

	unitsField["unitsToCollect"] = {}

	return unitsField
end
local sensorInfo = {
	name = "Determine defense positions",
	desc = "Get defense positions for unit count",
	author = "Flanopal",
	date = "2018-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @send units
return function(count, lineInfo, targetDistance, dispersion)
	local lane = lineInfo["lane"]

	if lane == -1 or count < 1 then
		return nil
	end

	local points = lineInfo["points"]
	local point = points[lane]["position"]

	local target = lineInfo["target"]
	local targetPoint = points[target]["position"]



	local targetDir = (point - targetPoint):Normalize()
	local perpendicular =  Vec3(targetDir.z, targetDir.y, -targetDir.x) * dispersion

	-- Get vector to defense position - in specific distance from target point and for specific dispersion of units
	local initPoint = targetPoint + targetDir * targetDistance
	local positions = { initPoint }

	local coef = 1
	for i = 1, count - 1 do
		positions[#positions + 1] = initPoint + (perpendicular * i * coef)
		coef = -coef 
	end
	return positions
end
local sensorInfo = {
	name = "Get enemy turret in radius",
	desc = "Go throught enemy units and find turrets, prefere shika",
	author = "Flanopal",
	date = "2019-05-09"
}

VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

attach.Module(modules, "message") 

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(position, radius)
	local enemyIDs = Sensors.core.EnemyTeamIDs()
	local turret = nil
	for id=1,#enemyIDs do
		-- If we found team that occupy lane, don't search through others
		if turren ~= nil then
			break;
		end

		local enemyUnits = Spring.GetUnitsInCylinder(position.x,position.z,radius, enemyIDs[id])
		for i=1,#enemyUnits do
			local unitDefID = Spring.GetUnitDefID(enemyUnits[i])
			if UnitDefs[unitDefID] ~= nil then
				local name = UnitDefs[unitDefID].name
				if name == 'shika' then	
					return enemyUnits[i]
				elseif name == 'corvipe' or name == 'corllt' then
					turret = enemyUnits[i]
				end
			end
		end
	end
	return turret
end
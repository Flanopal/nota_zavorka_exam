local sensorInfo = {
	name = "Update farc line point",
	desc = "Update point of line that should farc reclaim",
	author = "Flanopal",
	date = "2019-05-09"
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(lineInfo, linePoint)
	if linePoint < #lineInfo["points"] and linePoint < lineInfo["lane"] - 1 then
		return linePoint + 1
	end
	return linePoint
end
local sensorInfo = {
	name = "Determine defense tower positions",
	desc = "Get defense positions for towers",
	author = "Flanopal",
	date = "2018-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @send units
return function(towers, lineInfo)
	local positions = Sensors.nota_zavorka_exam.DetermineDefensePositions(#towers, lineInfo, 1450, 50)
	if positions == nil then
		return
	end

	for i = 1, #towers do
		towers[i]["toPos"] = positions[i]
	end
end
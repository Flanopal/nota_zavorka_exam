local sensorInfo = {
	name = "Get transport",
    desc = "From given transport table choose one transport to execute",
	author = "Flanopal"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(transports)
	
	for i=1, #transports do
		if transports[i]["tower"] ~= nil and transports[i]["toPos"] ~= nil then
			return transports[i]
		end
	end
	return nil
end
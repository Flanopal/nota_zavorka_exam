local sensorInfo = {
	name = "Calculate dead units",
	desc = "Go through table and calculate dead units",
	author = "Flanopal",
	date = "2019-05-09"
}

VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

attach.Module(modules, "message") 

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(table)
	local dead = 0
	for i=1,#table do
		if Spring.ValidUnitID(table[i]["unit"]) ~= true then
			dead = dead + 1
		end
	end
	return dead
end
local sensorInfo = {
	name = "Get cannon target position",
	desc = "Get position for cannon shooting, prefere turret position",
	author = "Flanopal",
	date = "2019-08-01",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @send units
return function(turret, default)
	if turret == nil then
		return default
	end
	return Vec3(Spring.GetUnitPosition(turret))
end
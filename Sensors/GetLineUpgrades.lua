local sensorInfo = {
	name = "Get line upgrades",
    desc = "Get upgrades that can be purchased for the line",
	author = "Flanopal"
}

return function(lineName, misInfo, agresive)
	local actualUpgrade = misInfo["battleLines"][lineName]["extraSpawnSize"]
	local dif = misInfo["upgrade"]["lineMaxLevel"] - actualUpgrade

	if dif > 5 then
		dif = 5
	end

	local price = dif * misInfo["upgrade"]["line"]
	
	if agresive == false then
		price = price + 200
	end

	if Spring.GetTeamResources(Spring.GetUnitTeam(units[1]),"metal") > price then
		return dif
	end
	return 0
end
local sensorInfo = {
	name = "CheckUnitAlive",
	desc = "Check if given unit in the table is alive",
	author = "Flanopal",
	date = "2019-05-09"
}

VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

attach.Module(modules, "message") 

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(table,key)
	if Spring.ValidUnitID(table[key]) ~= true then
		table[key] = nil
	end
end
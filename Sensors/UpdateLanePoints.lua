local sensorInfo = {
	name = "Update lane points",
	desc = "Update indices of points that are on lane",
	author = "Flanopal",
	date = "2019-05-09"
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(lineInfo, radius)
	local points = lineInfo["points"]
	local lane = lineInfo["lane"]
	local target = lineInfo["target"]

	if points[lane]["ownerAllyID"] == 1 then
		lineInfo["target"] = lane
		for i = lane - 1, 1 do
			if points[i]["isStrongpoint"] == true and points[i]["ownerAllyID"] == 0 then
				lineInfo["lane"] = i
				return false
			end
		end
	elseif points[target]["ownerAllyID"] == 0 or points[target]["ownerAllyID"] == nil then
		lineInfo["lane"] = target
		for i = target + 1, #points do
			if points[i]["isStrongpoint"] == true then
				if points[i]["ownerAllyID"] == 1 then
					lineInfo["target"] = i
					return false
				else
					lineInfo["lane"] = i
				end
			end
		end	

		-- No strongpoints infront, line is done
		lineInfo["target"] = -1
		lineInfo["lane"] = -1
		return true
	end
	return false
end
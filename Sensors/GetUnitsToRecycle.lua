local sensorInfo = {
	name = "Get units to recycle",
	desc = "Fill table with units ready to be recycled",
	author = "Flanopal",
	date = "2019-05-09"
}

VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

attach.Module(modules, "message") 

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(lineInfo)
	-- Get last point of the lane
	local point = lineInfo["points"][#lineInfo["points"]]["position"]

	local unitsToFilter = Sensors.nota_zavorka_exam.GetUnitsInRadius(point,1000)
	local myTeam = Spring.GetUnitTeam(units[1])

	for i=1,#unitsToFilter do
		if Spring.GetUnitTeam(unitsToFilter[i]) ~= myTeam then
			lineInfo["unitsToCollect"][#lineInfo["unitsToCollect"] + 1] = unitsToFilter[i]
		end
	end
end
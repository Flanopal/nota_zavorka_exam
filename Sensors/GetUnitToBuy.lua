local sensorInfo = {
	name = "GetUnitToBuy",
    desc = "Get description of unit we want to buy",
	author = "Flanopal"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(laneUnits, agresive)
	-- Prinary buy radar for lane
	if laneUnits["defenders"]["radar"] == nil then
		local path = {"defenders", "radar"}
		return { path = path, name = 'armseer' }
	end

	-- Get one farc to reclaim init units
	if agresive == true then
		if laneUnits["farcs"][1] == nil then
			local path = {"farcs", 1}
			return { path = path, name = 'armfark'}
		end
	else
		-- Prefere defenders on lanes without aggresive mode
		for i=1,#laneUnits["defenders"]["units"] do
			if laneUnits["defenders"]["units"][i]["unit"] == nil then
				local path = {"defenders", "units", i, "unit"}
				return { path = path, name = 'armmav' }
			end
		end
	end


	-- than artilery for fast destruction of first point
	if #laneUnits["squads"]["artilery"] < 1 then
		local path = {"squads", "artilery", #laneUnits["squads"]["artilery"] + 1}
		return { path = path, name = 'armmart' }
	end

	-- Fill defenders units per lane
	for i=1,#laneUnits["defenders"]["units"] do
		if laneUnits["defenders"]["units"][i]["unit"] == nil then
			local path = {"defenders", "units", i, "unit"}
			return { path = path, name = 'armmav' }
		end
	end

	
	--[[if laneUnits["defenders"]["atlas"] == nil then
		local path = {"defenders", "atlas"}
		return { path = path, name = 'armatlas' }
	end
	-- Buy defensive towers
	for i=1,#laneUnits["defenders"]["towers"] do
		if laneUnits["defenders"]["towers"][i]["tower"] == nil then
			local path = {"defenders", "towers", i, "tower"}
			return { path = path, name = 'armbox' }
		end
	end]]--

	-- Buy Farcks	
	for i=1,2 do
		if laneUnits["farcs"][i] == nil then
			local path = {"farcs", i}
			return { path = path, name = 'armfark'}
		end
	end


	if agresive == false and Spring.GetTeamResources(Spring.GetUnitTeam(units[1]),"metal") < 800 then 
		return nil
	end

	

	--[[if #laneUnits["squads"]["units"] < 20 then
		local path = {"squads", "units", #laneUnits["squads"]["units"] + 1}
		return { path = path, name = 'armzeus' }
	end--]]

	return nil
end
local sensorInfo = {
	name = "Get units in radius",
	desc = "Get units from the team in radius",
	author = "Flanopal",
	date = "2019-05-09"
}

VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

attach.Module(modules, "message") 

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(position, radius)
	local team = Spring.GetUnitTeam(units[1])
	return Spring.GetUnitsInCylinder(position.x,position.z,radius, team)
end
local sensorInfo = {
	name = "Create squad",
	desc = "From count of 10 units create one squad",
	author = "AzGhort",
	date = "2019-05-09"
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(soldiers)
	local squad = {}
	for i=1,10 do
		squad[i] = {}
		squad[i]["unit"] = soldiers[#soldiers]
		soldiers[#soldiers] = nil
	end
	return squad
end
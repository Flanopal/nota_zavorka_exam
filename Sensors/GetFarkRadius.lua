local sensorInfo = {
	name = "Get farc radius",
	desc = "Get radius for farc reclaim",
	author = "Flanopal",
	date = "2019-08-11"
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(lineInfo,point)
	return lineInfo["points"][point]["position"]:Distance(lineInfo["points"][point-1]["position"])
end
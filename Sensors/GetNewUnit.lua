local sensorInfo = {
	name = "Get new unit",
	desc = "From radius find unit, that was added - is not in prevUnits",
	author = "Flanopal",
	date = "2019-05-09"
}

VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

attach.Module(modules, "message") 

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(point,radius,prevUnits)
	local newUnits = Sensors.nota_zavorka_exam.GetUnitsInRadius(point,radius)
	for i=1,#newUnits do
		local present = false
		for j=1,#prevUnits do
			if newUnits[i] == prevUnits[j] then
				present = true
				break
			end
		end
		if present == false then
			return newUnits[i]
		end
	end
end
local sensorInfo = {
	name = "Should attack point",
	desc = "Decide if cannons should attack point or not",
	author = "Flanopal",
	date = "2019-05-09"
}

local EVAL_PERIOD_DEFAULT = -1 

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(lineInfo, attackPoint)
	local points = lineInfo["points"]

	if attackPoint > 0 and points[attackPoint]["ownerAllyID"] == 1 then
		return true
	end
	return false
end
local sensorInfo = {
	name = "Determine radar position",
	desc = "Get position for radar on the line",
	author = "Flanopal",
	date = "2018-05-13",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @send units
return function(lineInfo)
	local lane = lineInfo["lane"]

	if lane == -1 then
		return nil
	end

	local points = lineInfo["points"]
	local point = points[lane]["position"]
	local next = lane + 1

	local vector = points[next]["position"] - point

	local dx = vector.x / 10
	local dz = vector.z / 10

	local init = Vec3(point.x,point.y,point.z)
	init.x = init.x - dx * 2
	init.z = init.z - dz * 2

	return init
end
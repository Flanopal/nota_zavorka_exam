local sensorInfo = {
	name = "Devide init units",
    desc = "Move initial units into line positions or mark them for recycle",
	author = "Flanopal"
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(base, lineUnits1, lineUnits2)
	
	local unitsToDevide = Sensors.nota_zavorka_exam.GetUnitsInRadius(base,2000)
	for i=1,#unitsToDevide do
		local unitDefID = Spring.GetUnitDefID(unitsToDevide[i])
		local name = UnitDefs[unitDefID].name
		if name == 'armbox' then	
			--lineUnits1["defenders"]["towers"][1]["tower"] = unitsToDevide[i]
			lineUnits1["unitsToCollect"][#lineUnits1["unitsToCollect"] + 1] = unitsToDevide[i]
		elseif name == 'armatlas' then
			lineUnits1["unitsToCollect"][#lineUnits1["unitsToCollect"] + 1] = unitsToDevide[i]
			--[[if lineUnits1["defenders"]["atlas"] == nil then
				lineUnits1["defenders"]["atlas"] = unitsToDevide[i]
			else
				lineUnits1["defenders"]["atlas"] = unitsToDevide[i]
			end]]--
		elseif name == 'armmart' then
			lineUnits1["squads"]["artilery"][1] = unitsToDevide[i]
		elseif name ~= 'armmstor' then
			lineUnits1["unitsToCollect"][#lineUnits1["unitsToCollect"] + 1] = unitsToDevide[i]
		end
	end
end
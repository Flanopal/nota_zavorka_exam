local sensorInfo = {
	name = "Get trash at line point",
    desc = "Return metal that can be reclaimed around line point",
	author = "Flanopal"
}

return function(position, radius)
    local features = Spring.GetFeaturesInCylinder(position.x, position.z, radius)
    local metalFeatures = {}
    for i=1, #features do
        local remainingMetal, maxMetal, remainingEnergy, maxEnergy, reclaimLeft = Spring.GetFeatureResources(features[i])
        if remainingMetal > 0 then
            metalFeatures[#metalFeatures + 1] = features[i]
        end
    end
    return metalFeatures
end